<?php

use yii\db\Migration;

/**
 * Class m190901_152519_alter_users_email_column
 */
class m190901_152519_alter_users_email_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'email', $this->string(512)->null());
    }
}
