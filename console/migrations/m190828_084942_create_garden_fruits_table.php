<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%garden_apples}}`.
 */
class m190828_084942_create_garden_fruits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // TODO Придумать что-нибудь получше, хотя бы вынести в базовый класс
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%garden_fruits}}', [
            'id' => $this->primaryKey(),
            'type_code' => $this->string(16),
            'position_code' => $this->string(16),
            'integrity' => $this->tinyInteger(3),
            'color_hex' => $this->string(6),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'destructed_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%garden_fruits}}');
    }
}
