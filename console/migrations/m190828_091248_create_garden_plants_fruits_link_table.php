<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%garden_tress_apples_link}}`.
 */
class m190828_091248_create_garden_plants_fruits_link_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // TODO Придумать что-нибудь получше, хотя бы вынести в базовый класс
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%garden_plants_fruits_link}}', [
            'id' => $this->primaryKey(),
            'plant_id' => $this->integer(),
            'fruit_id' => $this->integer(),
            'state_code' => $this->string(16),
            'date_ground_fall' => $this->dateTime()->null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%garden_plants_fruits_link}}');
    }
}
