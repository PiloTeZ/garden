<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%garden_trees}}`.
 */
class m190828_084019_create_garden_plants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // TODO Придумать что-нибудь получше, хотя бы вынести в базовый класс
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%garden_plants}}', [
            'id' => $this->primaryKey(),
            'type_code' => $this->string(16),
            'name' => $this->string(128),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%garden_plants}}');
    }
}
