<?php

namespace common\modules\garden\components\plants;

use common\components\BaseComponentAction;
use common\modules\garden\models\fruit\Fruit;
use common\modules\garden\models\plant\Plant;
use common\modules\garden\models\plant\PlantFruitLink;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Привязать плод к растению
 */
class PlantFruitAdd extends BaseComponentAction
{
    /** @var Fruit Плод */
    public $fruit;

    /** @var Plant Растение */
    public $plant;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fruit', 'plant'], 'required'],
        ];
    }

    /**
     * @see isAvailable()
     * @inheritdoc
     */
    public function isAvailableInternal(): bool
    {
        if (!$this->fruit->isNewRecord) {
            // Нельзя привязать уже существующее яблоко
            return false;
        }

        if ($this->fruit->position_code) {
            // Плод уже расположен и не может быть перенесен на растение
            return false;
        }

        if (!$this->plant->checkFruitCompatibility($this->fruit)) {
            // Плод не совместим с указанным растением
            return false;
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function executeInternal()
    {
        $this->fruit->position_code = Fruit::POSITION_PLANT;
        if (!$this->fruit->save()) {
            throw new Exception('Не удалось сохранить плод ' . Json::encode($this->fruit->getErrors()));
        }

        $this->plant->link('fruits', $this->fruit, ['state_code' => PlantFruitLink::STATE_ON_PLANT]);
    }
}
