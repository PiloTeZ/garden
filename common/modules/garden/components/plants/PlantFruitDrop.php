<?php

namespace common\modules\garden\components\plants;

use common\components\BaseComponentAction;
use common\helpers\DateTimeHelper;
use common\modules\garden\models\fruit\Fruit;
use common\modules\garden\models\plant\PlantFruitLink;
use Yii;
use yii\base\Exception;
use yii\helpers\Json;

/**
 * Сбросить плод с растения
 */
class PlantFruitDrop extends BaseComponentAction
{
    /** @var Fruit */
    public $fruit;

    /**
     * @param Fruit $fruit
     * @param array $config
     */
    public function __construct(Fruit $fruit, array $config = [])
    {
        parent::__construct($config);

        $this->fruit = $fruit;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['fruit', 'required'],
        ];
    }

    /**
     * @see isAvailable()
     */
    public function isAvailableInternal(): bool
    {
        if ($this->fruit->position_code !== Fruit::POSITION_PLANT) {
            // Плод не на растении
            return false;
        }

        if (!$this->fruit->plant) {
            // Плод не привязан к растению
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     * @throws \Throwable
     */
    public function executeInternal()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->fruit->position_code = Fruit::POSITION_GROUND;
            if (!$this->fruit->save()) {
                throw new Exception('Не удалось сохранить расположение плода ' . Json::encode($this->fruit->getErrors()));
            }

            $this->fruit->plantLink->state_code = PlantFruitLink::STATE_RIPPED;
            $this->fruit->plantLink->date_ground_fall = DateTimeHelper::formatDateToDb();
            if (!$this->fruit->plantLink->save()) {
                throw new Exception('Не удалось сохранить дату падения плода' . Json::encode($this->fruit->plantLink->getErrors()));
            }

            $transaction->commit();
        } catch (\Throwable $exception) {
            $transaction->rollBack();

            throw $exception;
        }
    }
}
