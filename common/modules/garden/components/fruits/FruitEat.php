<?php

namespace common\modules\garden\components\fruits;

use common\components\BaseComponentAction;
use common\helpers\DateTimeHelper;
use common\modules\garden\models\fruit\Fruit;
use yii\base\Exception;

class FruitEat extends BaseComponentAction
{
    /** @var Fruit */
    public $fruit;

    /** @var int $percent */
    public $percent;

    public function rules()
    {
        return [
            [['fruit', 'percent'], 'required'],
            ['percent', 'integer'],
        ];
    }

    /**
     * @see isAvailable()
     */
    protected function isAvailableInternal(): bool
    {
        if ($this->fruit->position_code != Fruit::POSITION_GROUND) {
            return false;
        }

        if ($this->fruit->isEaten()) {
            return false;
        }

        // TODO
//        if ($this->fruit->decay()->isDecay()) {
//            return false;
//        }

        return true;
    }

    /**
     * @see execute()
     * @throws Exception
     */
    protected function executeInternal()
    {
        $this->setFruitIntegrity();
        $this->setFruitEatenDateIfThat();

        if (!$this->fruit->save()) {
            throw new Exception('Не удалось обновить состояние плода');
        }
    }

    private function setFruitIntegrity()
    {
        $eatPercent = $this->percent > $this->fruit->integrity
            ? $this->fruit->integrity
            : $this->percent;

        $this->fruit->integrity -= $eatPercent;
    }

    protected function setFruitEatenDateIfThat()
    {
        if ($this->fruit->integrity == 0) {
            $this->fruit->destructed_at = DateTimeHelper::formatDateToDb();
        }
    }
}
