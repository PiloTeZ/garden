<?php

namespace common\modules\garden\components\fruits;

use common\modules\garden\models\fruit\Fruit;
use DateTime;
use yii\base\BaseObject;

/**
 * Гниение
 */
class FruitDecay extends BaseObject
{
    /** @var Fruit */
    private $fruit;

    public function __construct(Fruit $fruit, array $config = [])
    {
        parent::__construct($config);
        $this->fruit = $fruit;
    }

    /**
     * Рассчитать процент гнилости
     * @return int
     */
    public function calc()
    {
        if ($this->fruit->position_code === Fruit::POSITION_PLANT) {
            return 0;
        }

        $decayDateStart = new DateTime($this->fruit->plantLink->date_ground_fall);
        $decayDateEnd = new DateTime($this->fruit->destructed_at ?: null);
        $timeSpent = $decayDateEnd->getTimestamp() - $decayDateStart->getTimestamp();
        $decayDuration = $this->fruit->getProperties()->decayDuration;
        $decay = $timeSpent / ($decayDuration / 100);

        return round($decay, 2);
    }

    /**
     * Сгнил ли плод
     * @return bool
     */
    public function isDecay()
    {
        return $this->calc() == 100;
    }
}
