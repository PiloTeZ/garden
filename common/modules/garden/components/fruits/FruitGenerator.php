<?php

namespace common\modules\garden\components\fruits;

use common\modules\garden\models\fruit\Fruit;
use common\modules\garden\models\plant\Plant;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Генератор плодов
 */
class FruitGenerator extends Model
{
    /** @var integer Цвет. По умолчанию сгенерится автоматически */
    public $colorHex = null;

    /** @var integer Растение */
    public $plantId;

    /** @var integer Количество */
    public $number;

    /** @var string Тип */
    public $typeCode;

    /** @var Plant */
    private $plant;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['number', 'typeCode', 'plantId'], 'required'],
            [['typeCode'], 'string'],
            [['plantId'], 'integer'],
            [['number'], 'integer', 'max' => 10],
        ]);
    }

    /**
     * Сгенерировать плоды
     * @throws Exception
     * @throws \Throwable
     * @return true
     */
    public function execute(): bool
    {
        if (!$this->validate()) {
            throw new Exception('Передан неверный набор параметров ' . Json::encode($this->getErrors()));
        }

        $transaction = Fruit::getDb()->beginTransaction();
        try {
            $this->initPlant();
            $this->createFruitsOnPlant();

            $transaction->commit();
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return true;
    }

    /**
     * Создать плоды
     * @throws Exception
     */
    public function createFruitsOnPlant()
    {
        for ($i = 1; $i <= $this->number; $i++) {
            $this->createFruitOnPlant();
        }
    }

    /**
     * Создать плод
     * @throws Exception
     */
    private function createFruitOnPlant()
    {
        $fruit = new Fruit();
        $fruit->type_code = $this->typeCode;
        $fruit->color_hex = $this->colorHex;

        $this->plant->addFruit($fruit)->execute();
    }

    /**
     * Растение
     */
    private function initPlant()
    {
        $this->plant = Plant::findOne($this->plantId);
    }
}
