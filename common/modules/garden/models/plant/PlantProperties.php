<?php

namespace common\modules\garden\models\plant;

use yii\base\BaseObject;

class PlantProperties extends BaseObject
{
    public $label;
    public $code;
    public $compatibleFruits;
}
