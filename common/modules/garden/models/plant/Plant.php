<?php

namespace common\modules\garden\models\plant;

use common\modules\garden\components\plants\PlantFruitAdd;
use common\modules\garden\components\plants\PlantFruitDrop;
use common\modules\garden\models\fruit\Fruit;
use Yii;
use common\behaviors\TimestampBehavior;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

// TODO Удаление модели и зависимостей

/**
 * Растение
 *
 * @property int $id
 * @property string $type_code Тип
 * @property string $name Название
 * @property string $created_at Дата создания
 * @property string $updated_at Дата обновления
 *
 * @property Fruit[] $fruits
 */
class Plant extends \yii\db\ActiveRecord
{
    /** @var string Яблоня */
    const TYPE_APPLE_TREE = 'apple_tree';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'garden_plants';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_code'], 'in', 'range' => ArrayHelper::getColumn(static::getTypes(), 'code')],
            [['name', 'type_code'], 'required'],
            [['name'], 'string', 'max' => 128],
            [['type_code'], 'string', 'max' => 16],
        ];
    }

    public function beforeDelete()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($this->fruits as $fruit) {
                if ($fruit->delete() === false) {
                    throw new Exception('Не удален');
                }
            }

            $transaction->commit();
        } catch (\Throwable $exception) {
            $transaction->rollBack();

            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_code' => 'Type code',
            'name' => 'Name',
        ];
    }

    /**
     * Типы
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_APPLE_TREE => new PlantProperties([
                'label' => 'Яблоня',
                'code' => Plant::TYPE_APPLE_TREE,
                'compatibleFruits' => [Fruit::TYPE_APPLE],
            ]),
        ];
    }

    /**
     * Свойства растений
     * @return PlantProperties
     */
    public function getProperties(): PlantProperties
    {
        return ArrayHelper::getValue(
            static::getTypes(),
            $this->type_code,
            new PlantProperties()
        );
    }

    /**
     * Типы в формате code => label
     * @return array
     */
    public static function getTypesAsItems(): array
    {
        return ArrayHelper::map(static::getTypes(), 'code', 'label');
    }

    /**
     * Проверить совместимость растения и плода
     * @param Fruit $fruit
     * @return boolean
     */
    public function checkFruitCompatibility(Fruit $fruit)
    {
        return in_array($fruit->type_code, $this->getProperties()->compatibleFruits);
    }

    public function getFruitsLink()
    {
        return $this->hasMany(PlantFruitLink::class, ['plant_id' => 'id']);
    }

    public function getFruits()
    {
        return $this
            ->hasMany(Fruit::class, ['id' => 'fruit_id'])
            ->via('fruitsLink');
    }

    /**
     * Добавить плод
     * @param Fruit $fruit
     * @return PlantFruitAdd
     */
    public function addFruit(Fruit $fruit): PlantFruitAdd
    {
        $plantAddFruit = new PlantFruitAdd();
        $plantAddFruit->fruit = $fruit;
        $plantAddFruit->plant = $this;

        return $plantAddFruit;
    }

    /**
     * Сбросить плод
     * @param Fruit $fruit
     * @return PlantFruitDrop
     */
    public function dropFruit(Fruit $fruit): PlantFruitDrop
    {
        return new PlantFruitDrop($fruit);
    }
}
