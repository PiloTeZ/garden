<?php

namespace common\modules\garden\models\plant;

use Yii;

/**
 * Связь между растение и плодом
 *
 * @property int $id
 * @property int $plant_id Растение
 * @property int $fruit_id Плод
 * @property int $state_code Состояние
 * @property string $date_ground_fall Дата отрыва от растения
 */
class PlantFruitLink extends \yii\db\ActiveRecord
{
    /** @var string На растении */
    const STATE_ON_PLANT = 'on_plant';
    /** @var string Сорвано (не обязательно руками) */
    const STATE_RIPPED = 'ripped';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'garden_plants_fruits_link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plant_id', 'fruit_id', 'state_code'], 'required'],
            [['plant_id', 'fruit_id'], 'integer'],
            [['state_code'], 'string', 'max' => 16],
            [['date_ground_fall'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plant_id' => 'Plant ID',
            'fruit_id' => 'Fruit ID',
            'state_code' => 'State code',
            'date_ground_fall' => 'Date Ground Fall',
        ];
    }
}
