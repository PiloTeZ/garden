<?php

namespace common\modules\garden\models\fruit;

use common\modules\garden\components\fruits\FruitDecay;
use common\modules\garden\components\fruits\FruitEat;
use common\modules\garden\components\plants\PlantFruitDrop;
use common\modules\garden\models\plant\Plant;
use common\modules\garden\models\plant\PlantFruitLink;
use Yii;
use common\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

// TODO Удаление модели и зависимостей

/**
 * Плод растения
 *
 * @property int $id
 * @property string $type_code Тип плода
 * @property string $position_code Расположение
 * @property int $integrity Целостность яблока (сколько процентов осталось)
 * @property string $color_hex Цвет в HEX 000000
 * @property string $created_at Дата создания
 * @property string $updated_at Дата обновления
 * @property string $destructed_at Дата уничтожения (например съедения)
 *
 * @property Plant $plant
 * @property PlantFruitLink $plantLink
 */
class Fruit extends \yii\db\ActiveRecord
{
    /** @var string Яблоко */
    const TYPE_APPLE = 'apple';

    /** @var int На растении */
    const POSITION_PLANT = 'plant';

    /** @var int На земле */
    const POSITION_GROUND = 'ground';

    /** @var int Целостность по умолчанию */
    public $integrityDefault = 100;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'garden_fruits';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['integrity'], 'default', 'value' => $this->integrityDefault],
            [['color_hex'], 'default', 'value' => $this->generateColor()],
            [['integrity', 'type_code'], 'required'],
            [['type_code'], 'in', 'range' => ArrayHelper::getColumn(static::getTypes(), 'code')],
            [['type_code'], 'string', 'max' => 16],
            [['integrity'], 'integer', 'min' => 0, 'max' => 100],
            [['position_code'], 'string', 'max' => 16],
            [['color_hex'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_code' => 'Тип',
            'position_code' => 'Расположение',
            'integrity' => 'Целостность',
            'color_hex' => 'Цвет',
            'created_at' => 'Дата создания',
            'destructed_at' => 'Дата исчезновения',
        ];
    }

    public function attributeHints()
    {
        return [
            'color_hex' => 'Цвет в формате HEX #00000',
        ];
    }

    public function getColor()
    {
        return $this->decay()->isDecay()
            ? $this->getProperties()->decayColor
            : '#' . $this->color_hex;
    }

    /**
     * Типы
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_APPLE => new FruitProperties(
                [
                    'code' => self::TYPE_APPLE,
                    'label' => 'Яблоко',
                    'decayDuration' => 5 * 3600,
                    'decayColor' => '#964b00',
                ]
            ),
        ];
    }

    /**
     * Свойства плода
     * @return FruitProperties
     */
    public function getProperties(): FruitProperties
    {
        return ArrayHelper::getValue(
            static::getTypes(),
            $this->type_code,
            new FruitProperties()
        );
    }

    /**
     * Типы в формате code => label
     * @return array
     */
    public static function getTypesAsItems(): array
    {
        return ArrayHelper::map(static::getTypes(), 'code', 'label');
    }

    /**
     * Позиции
     * @return array
     */
    public static function getPositions(): array
    {
        return [
            self::POSITION_PLANT => [
                'code' => self::POSITION_PLANT,
                'label' => 'На растении',
            ],
            self::POSITION_GROUND => [
                'code' => self::POSITION_GROUND,
                'label' => 'На земле',
            ],
        ];
    }

    public function getPositionLabel()
    {
        return ArrayHelper::getValue($this->getPositions(), $this->position_code . '.label', 'неизвестно');
    }

    /**
     * Позиции в формате code => label
     * @return array
     */
    public static function getPositionsAsItems(): array
    {
        return ArrayHelper::map(static::getTypes(), 'code', 'label');
    }

    /**
     * Сгенерировать случайный цвет
     * TODO По-хорошему вынести в хелпер
     * @return bool|string
     */
    public function generateColor()
    {
        return substr(str_shuffle('0123456789ABCDEF'), 0, 6);
    }

    // TODO Учитывать связи при удалении

    public function getPlantLink()
    {
        return $this->hasOne(PlantFruitLink::class, ['fruit_id' => 'id']);
    }

    public function getPlant()
    {
        return $this
            ->hasOne(Plant::class, ['id' => 'plant_id'])
            ->via('plantLink');
    }

    /**
     * Гниение
     * @return FruitDecay
     */
    public function decay()
    {
        return new FruitDecay($this);
    }

    /**
     * Трапеза
     * @param $percent
     * @return FruitEat
     */
    public function eat($percent): FruitEat
    {
        $fruitEat = new FruitEat();
        $fruitEat->fruit = $this;
        $fruitEat->percent = $percent;

        return $fruitEat;
    }

    /**
     * Съедено ли
     * @return bool
     */
    public function isEaten()
    {
        if ($this->integrity == 0) {
            return true;
        }

        if ($this->destructed_at) {
            return true;
        }

        return false;
    }

    /**
     * Сбросить плод с растения
     */
    public function dropFromPlant(): PlantFruitDrop
    {
        return $this->plant->dropFruit($this);
    }
}
