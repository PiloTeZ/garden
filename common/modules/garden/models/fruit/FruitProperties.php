<?php

namespace common\modules\garden\models\fruit;

use yii\base\BaseObject;

class FruitProperties extends BaseObject
{
    public $label;
    public $code;
    public $decayDuration;
    public $decayColor;
}
