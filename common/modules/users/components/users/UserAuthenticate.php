<?php

namespace common\modules\users\components\users;

use common\components\BaseComponentAction;
use common\modules\users\models\user\User;

/**
 * Аутентификация пользователя (поиск логина на наличие в БД и сверка пароля)
 */
class UserAuthenticate extends BaseComponentAction
{
    public $login;

    public $password;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'string'],
        ];
    }

    /**
     * @see isAvailable()
     */
    protected function isAvailableInternal(): bool
    {
        return true;
    }

    /**
     * @see execute()
     */
    protected function executeInternal()
    {
        $user = User::findOne(['username' => $this->login, 'status' => User::STATUS_ACTIVE]);
        if (!$user) {
            return false;
        }

        if (!\Yii::$app->security->validatePassword($this->password, $user->password_hash)) {
            return false;
        }

        return $user;
    }
}
