<?php

namespace common\modules\users\components\users;

use common\components\BaseComponentAction;
use common\modules\users\models\user\User;

/**
 * Авторизация пользователя без проверки пароля.
 * Используется например в админке или после регистрации пользователя
 */
class UserLoginForce extends BaseComponentAction
{
    /** @var User */
    public $user;

    public $isRemember = false;

    public function rules()
    {
        return [
            ['user', 'required'],
            ['isRemember', 'boolean'],
        ];
    }

    /**
     * @see isAvailable()
     */
    protected function isAvailableInternal(): bool
    {
        return \Yii::$app->user->isGuest;
    }

    /**
     * @see execute()
     */
    protected function executeInternal()
    {
        return \Yii::$app->user->login($this->user, $this->isRemember ? UserLogin::LONG_DURATION : 0);
    }
}
