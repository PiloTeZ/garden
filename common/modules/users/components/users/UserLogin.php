<?php

namespace common\modules\users\components\users;

use common\components\BaseComponentAction;

/**
 * Авторизация
 */
class UserLogin extends BaseComponentAction
{
    const LONG_DURATION = 3600 * 24 * 30;

    public $login;

    public $password;

    public $isRemember = false;

    public function rules()
    {
        return [
            [['login', 'password', 'isRemember'], 'required'],
            [['login', 'password'], 'string'],
            [['isRemember'], 'boolean'],
        ];
    }

    /**
     * @see isAvailable()
     */
    protected function isAvailableInternal(): bool
    {
        return \Yii::$app->user->isGuest;
    }

    /**
     * @see execute()
     * @throws \yii\base\Exception
     */
    protected function executeInternal()
    {
        $authenticate = new UserAuthenticate();
        $authenticate->login = $this->login;
        $authenticate->password = $this->password;
        $user = $authenticate->execute();
        if (!$user) {
            return false;
        }

        $loginForce = new UserLoginForce();
        $loginForce->user = $user;
        $loginForce->isRemember = $this->isRemember;

        return $loginForce->execute();
    }
}
