<?php

namespace common\modules\users\components\users;

use common\components\BaseComponentAction;
use common\modules\users\models\user\User;
use Yii;

/**
 * Зарегистрировать пользователя
 */
class UserRegister extends BaseComponentAction
{
    public $login;

    public $password;

    /** @var \common\modules\users\models\user\User */
    private $user;

    /**
     * @see isAvailable()
     * @throws \yii\base\Exception
     */
    protected function isAvailableInternal(): bool
    {
        $this->user = new User();
        $this->user->username = $this->login;
        $this->user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
        $this->user->auth_key = Yii::$app->security->generateRandomString();

        return $this->user->validate();
    }

    /**
     * @see execute()
     */
    protected function executeInternal()
    {
        return $this->user->save();
    }
}
