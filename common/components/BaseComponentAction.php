<?php

namespace common\components;

use yii\base\Exception;
use yii\base\Model;

/**
 * Базовый класс для компонентов.
 * Пример использования реализации:
 *
 * ```
 * $register = new UserRegister();
 * $register->login = $this->login;
 * $register->password = $this->password;
 * $user = $register->execute();
 * ```
 */
abstract class BaseComponentAction extends Model
{
    /**
     * Доступно ли действие
     */
    final public function isAvailable(): bool
    {
        if (!$this->validate()) {
//            throw new Exception('Передан неверный набор параметров');
            return false;
        }

        return $this->isAvailableInternal();
    }

    /**
     * @see isAvailable()
     */
    abstract protected function isAvailableInternal(): bool;

    /**
     * Выполнить действие
     * @return mixed
     * @throws Exception
     */
    final public function execute()
    {
        if (!$this->isAvailable()) {
            throw new Exception('Действие недоступно');
        }

        return $this->executeInternal();
    }

    /**
     * @see execute()
     */
    abstract protected function executeInternal();
}
