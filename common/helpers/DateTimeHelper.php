<?php

namespace common\helpers;

class DateTimeHelper
{
    static public function formatDateToDb(): string
    {
        return date('Y-m-d H:i:s');
    }
}
