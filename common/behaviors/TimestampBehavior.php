<?php

namespace common\behaviors;

use common\helpers\DateTimeHelper;

/**
 * Вписывает datetime вместо unix timestamp
 */
class TimestampBehavior extends \yii\behaviors\TimestampBehavior
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (!$this->value) {
            $this->value = DateTimeHelper::formatDateToDb();
        }
    }
}
