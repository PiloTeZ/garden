<?php

namespace backend\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

class BaseBackendController extends Controller
{
    final public function behaviors()
    {
        return array_merge([
            [
                'class' => AccessControl::class,
                'except' => ['login', 'register'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ], $this->otherBehaviors());
    }

    public function otherBehaviors()
    {
        return [];
    }
}
