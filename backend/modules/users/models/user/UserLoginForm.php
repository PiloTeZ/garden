<?php

namespace backend\modules\users\models\user;

use common\modules\users\components\users\UserLogin;
use yii\base\Exception;
use yii\base\Model;

class UserLoginForm extends Model
{
    public $login;

    public $password;

    public $isRemember = true;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'string'],
            [['isRemember'], 'boolean'],
        ];
    }

    /**
     * @throws Exception
     */
    public function login(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $login = new UserLogin();
        $login->login = $this->login;
        $login->password = $this->password;
        $login->isRemember = $this->isRemember;
        if (!$login->execute()) {
            return false;
        }

        return true;
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'isRemember' => 'Запомнить меня',
        ];
    }
}
