<?php

namespace backend\modules\users\models\user;

use common\modules\users\components\users\UserLogin;
use common\modules\users\components\users\UserLoginForce;
use common\modules\users\components\users\UserRegister;
use yii\base\Exception;
use yii\base\Model;

class UserRegisterForm extends Model
{
    public $login;

    public $password;

    public $passwordRepeat;

    public function rules()
    {
        return [
            [['login', 'password', 'passwordRepeat'], 'required'],
            [['login', 'password', 'passwordRepeat'], 'string'],
            [['password'], 'compare', 'compareAttribute' => 'passwordRepeat', 'operator' => '=='],
        ];
    }

    /**
     * @throws Exception
     */
    public function register(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        $register = new UserRegister();
        $register->login = $this->login;
        $register->password = $this->password;
        $user = $register->execute();
        if (!$user) {
            return false;
        }

        $loginForce = new UserLoginForce();
        $loginForce->user = $user;
        $loginForce->execute();

        return true;
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'passwordRepeat' => 'Повтор пароля',
        ];
    }
}
