<?php

namespace backend\modules\users\controllers;

use backend\controllers\BaseBackendController;
use backend\modules\users\models\user\UserLoginForm;
use backend\modules\users\models\user\UserRegisterForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class UserController extends BaseBackendController
{
    /**
     * {@inheritdoc}
     */
    public function otherBehaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'register'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionLogin()
    {
        $loginForm = new UserLoginForm();

        if ($loginForm->load(\Yii::$app->request->post()) && $loginForm->login()) {
            return $this->redirect('/');
        }

        return $this->render('login', ['model' => $loginForm]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionRegister()
    {
        $form = new UserRegisterForm();

        if ($form->load(\Yii::$app->request->post()) && $form->register()) {
            return $this->redirect('/');
        }

        return $this->render('register', ['model' => $form]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        // TODO Вынести в компонент
        \Yii::$app->user->logout();

        return $this->goHome();
    }
}
