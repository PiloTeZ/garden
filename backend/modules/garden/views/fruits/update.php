<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\garden\models\fruit\Fruit */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update Fruit: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fruits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fruit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="fruit-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'color_hex')->textInput(['maxlength' => true, 'placeholder' => 'Сгенерировать автоматически']) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
