<?php

use yii\helpers\Html;
use common\modules\garden\models\plant\Plant;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\garden\models\fruit\Fruit */

$this->title = 'Create Fruit';
$this->params['breadcrumbs'][] = ['label' => 'Fruits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fruit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="fruit-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'plantId')->dropDownList(ArrayHelper::map(Plant::find()->asArray()->all(), 'id', 'name')) ?>

        <?= $form->field($model, 'colorHex')->textInput(['maxlength' => true, 'placeholder' => 'Сгенерировать автоматически']) ?>

        <?= $form->field($model, 'number')->input('number') ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
