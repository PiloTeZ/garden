<?php

namespace backend\modules\garden\controllers;

use backend\controllers\BaseBackendController;
use backend\modules\garden\models\fruit\FruitEditForm;
use backend\modules\garden\models\fruit\FruitAddForm;
use Yii;
use common\modules\garden\models\fruit\Fruit;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FruitsController implements the CRUD actions for Fruit model.
 */
class FruitsController extends BaseBackendController
{
    /**
     * {@inheritdoc}
     */
    public function otherBehaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fruit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fruit::find()->with('plant'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Fruit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fruit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new FruitAddForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->generate()) {
                Yii::$app->session->addFlash('success', 'Плоды успешно сгенерированы');

                return $this->redirect(['index']);
            } else {
                Yii::$app->session->addFlash('danger', 'Ошибка при генерации плодов');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Fruit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = FruitEditForm::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Fruit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function findModel($id)
    {
        $model = Fruit::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
