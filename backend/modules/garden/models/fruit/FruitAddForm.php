<?php

namespace backend\modules\garden\models\fruit;

use common\modules\garden\components\fruits\FruitGenerator;
use common\modules\garden\models\fruit\Fruit;
use yii\base\Model;

/**
 * Форма генератора плодов
 */
class FruitAddForm extends Model
{
    /** @var string Растение */
    public $plantId;

    /** @var string Цвет */
    public $colorHex;

    /** @var int Кол-во яблок */
    public $number = 1;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'plantId'], 'required'],
            [['number'], 'integer', 'max' => 10],
            [['colorHex'], 'string', 'min' => 3, 'max' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'colorHex' => 'Цвет',
            'number' => 'Количество',
            'plantId' => 'Растение',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'color_hex' => 'Цвет в формате HEX #00000',
        ];
    }

    /**
     * @throws \yii\base\Exception
     * @throws \Throwable
     */
    public function generate()
    {
        $generator = new FruitGenerator();
        $generator->plantId = $this->plantId;
        $generator->colorHex = $this->colorHex;
        $generator->typeCode = Fruit::TYPE_APPLE;
        $generator->number = $this->number;

        return $generator->execute();
    }
}
