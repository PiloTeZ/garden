<?php
namespace backend\modules\garden\models\fruit;

use common\modules\garden\models\fruit\Fruit;

class FruitEditForm extends Fruit
{
    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => ['color_hex'],
        ];
    }
}
