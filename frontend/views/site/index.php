<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $fruits \common\modules\garden\models\fruit\Fruit[] */
/* @var $fruit \common\modules\garden\models\fruit\Fruit */
$this->title = 'Сад';

// TODO Экранирование
// TODO Хелперы Yii
?>

<div class="row">
    <?php foreach ($fruits as $fruit) { ?>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div style="margin-bottom: 20px;" class="clearfix">
                        <div style="font-size: 36px; color: <?= $fruit->getColor() ?>; margin-right: 20px;"
                             class="pull-left"><?= Html::icon('certificate') ?></div>
                        <div class="pull-left" style="font-size: 12px">
                            ID: <?= $fruit->id ?><br>
                            Дерево: <?= $fruit->plant->name ?><br>
                            Расположение: <?= $fruit->getPositionLabel() ?><br>
                            Целостность: <?= $fruit->isEaten() ? 'съедено' : $fruit->integrity . '%' ?><br>
                            Гниение: <?= $fruit->decay()->isDecay() ? 'гнилое' : $fruit->decay()->calc() . '%' ?>
                        </div>
                    </div>
                    <?= Html::a(
                        'Сорвать',
                        ['drop-fruit', 'id' => $fruit->id],
                        [
                            'class' => 'btn btn-default btn-block',
                            'disabled' => !$fruit->dropFromPlant()->isAvailable(),
                        ]
                    ) ?>

                    <?php $form = ActiveForm::begin(['method' => 'post', 'action' => Url::to(['eat', 'id' => $fruit->id])]); ?>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6" style="padding-right: 0 !important;">
                            <?= Html::input('number', 'percent', 25, ['class' => 'form-control']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= Html::submitButton('Откусить', [
                                'class' => 'btn btn-default pull-right',
                                'name' => 'login-button',
                                'style' => 'width: 100% !important',
                                'disabled' => !$fruit->eat(0)->isAvailable(),
                            ]) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
